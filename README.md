# Pocket Highstreet Frontend
An app that lets you store public and private todo items. Anyone can tick off or delete public todos, but only authenticated users can add todos.
When adding a todo, make sure you chose the correct privacy setting.
After you add a todo, press the orange refresh button to see it.

## Setup
### Installing
```
git clone https://gitlab.com/pocket-highstreet/frontend.git
cd frontend
npm install
```

### Run the tests
```
npm test
```
Press `a` to run all tests.
press `q` to quit after tests have been run.

### Launch the app
Before you launch, make sure the [backend](https://gitlab.com/pocket-highstreet/backend) is running on port 8080.
```
npm start
```
Point your browser to `http://localhost3000/` and you should find the app there

