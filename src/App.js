import React from "react";
import { Router } from "react-router";
import { Route, Redirect } from "react-router-dom";
import history from "./history";
import Callback from "./Callback/Callback";
import Auth from "./Auth/Auth";
import TopBar from "./TopBar";
import PublicTodos from "./PublicTodos";
import PrivateTodos from "./PrivateTodos";

const auth = new Auth();

const handleAuthentication = ({ location }) => {
  if (/access_token|id_token|error/.test(location.hash)) {
    auth.handleAuthentication();
  }
};

function App() {
  return (
    <Router history={history}>
      <div>
        <Route path="/" render={props => <TopBar auth={auth} {...props} />} />
        <Route
          exact
          path="/"
          render={props => <PublicTodos auth={auth} {...props} />}
        />
        <Route
          exact
          path="/private"
          render={props =>
            auth.isAuthenticated() ? (
              <PrivateTodos auth={auth} {...props} />
            ) : (
              <Redirect to={"/"} />
            )
          }
        />
        <Route
          path="/callback"
          render={props => {
            handleAuthentication(props);
            return <Callback {...props} />;
          }}
        />
      </div>
    </Router>
  );
}

export default App;
