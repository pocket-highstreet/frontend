import React from "react";

function TodoList(props) {
  const { todos, ...otherProps } = props;
  return (
    <div className="todolist py-5 w-full flex flex-wrap items-center justify-center">
      {props.todos.map(todo => (
        <ToDo key={todo.id} todo={todo} {...otherProps} />
      ))}
    </div>
  );
}

function ToDo(props) {
  const { todo, toggleDone, deleteTodo } = props;
  return (
    <div className="todo w-2/3 py-3">
      <input
        className="mx-2"
        type="checkbox"
        onChange={toggleDone && (() => toggleDone(todo))}
        checked={todo.done}
      />
      <span className="text-lg pr-5">{todo.note}</span>

      {deleteTodo && (
        <button
          className="mx-5 px-2 py-1 bg-red-dark rounded-lg shadow hover:shadow-md"
          onClick={() => deleteTodo(todo)}
        >
          Delete
        </button>
      )}
    </div>
  );
}

export default TodoList;
