import React from "react";
import ReactDOM from "react-dom";
import { render, fireEvent, cleanup } from "react-testing-library";
import { BrowserRouter as Router } from "react-router-dom";
import "jest-dom/extend-expect";
import TopBar from "./TopBar";

afterEach(cleanup);

// Mock authentication, always returning signed in status
const auth = { isAuthenticated: () => true };
const authOUT = { isAuthenticated: () => false };

describe("When signed in, the top bar component", () => {
  it("renders an input textfield with a label", () => {
    const expectedLabel = "New todo";

    const { getByLabelText } = render(
      <Router>
        <TopBar auth={auth} />{" "}
      </Router>
    );
    const inputField = getByLabelText(expectedLabel);

    expect(inputField).toBeInTheDocument();
  });

  it("renders a button to add a todo", () => {
    const buttonText = "Add Todo";

    const { getByText } = render(
      <Router>
        <TopBar auth={auth} />{" "}
      </Router>
    );
    const addButton = getByText(buttonText);

    expect(addButton).toBeInTheDocument();
  });

  it("renders a privacy checbox", () => {
    const privacyCheckboxLabel = "Private";

    const { getByLabelText } = render(
      <Router>
        <TopBar auth={auth} />{" "}
      </Router>
    );
    const privacyCheckbox = getByLabelText(privacyCheckboxLabel);

    expect(privacyCheckbox).toBeInTheDocument();
  });
});

describe("When signed in, the privacy checkbox", () => {
  const PRIVACYCHECKBOXLABEL = "Private";

  it("is visible", () => {
    const { getByLabelText } = render(
      <Router>
        <TopBar auth={auth} />{" "}
      </Router>
    );
    const privacyCheckbox = getByLabelText(PRIVACYCHECKBOXLABEL);

    expect(privacyCheckbox).toBeVisible();
  });

  it("starts with off by default", () => {
    const { getByLabelText } = render(
      <Router>
        <TopBar auth={auth} />{" "}
      </Router>
    );
    const privacyCheckbox = getByLabelText(PRIVACYCHECKBOXLABEL);

    expect(privacyCheckbox.checked).toBe(false);
  });

  it("toggles when clicked", () => {
    const { getByLabelText } = render(
      <Router>
        <TopBar auth={auth} />{" "}
      </Router>
    );
    const privacyCheckbox = getByLabelText(PRIVACYCHECKBOXLABEL);

    expect(privacyCheckbox.checked).toBe(false);
    fireEvent.click(privacyCheckbox);

    expect(privacyCheckbox.checked).toBe(true);
  });
});

describe("When signed out, the privacy checkbox", () => {
  const PRIVACYCHECKBOXLABEL = "Private";

  it("is not rendered", () => {
    const { queryByLabelText } = render(
      <Router>
        <TopBar auth={authOUT} />{" "}
      </Router>
    );
    const privacyCheckbox = queryByLabelText(PRIVACYCHECKBOXLABEL);

    expect(privacyCheckbox).not.toBeInTheDocument();
  });
});
