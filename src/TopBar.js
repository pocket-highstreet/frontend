import React from "react";
import { Link } from "react-router-dom";
import history from "./history";

class TopBar extends React.Component {
  state = {
    newTodoText: "",
    priv: false
  };

  async addTodo() {
    const signedIn = this.props.auth.isAuthenticated();
    if (signedIn) {
      const ACCESS_TOKEN = this.props.auth.getAccessToken();
      const { newTodoText } = this.state;
      if (newTodoText && signedIn) {
        const todo = {
          note: this.state.newTodoText,
          done: false,
          priv: this.state.priv
        };
        const response = await fetch("http://localhost:8080/private", {
          mode: "cors",
          method: "POST",
          headers: {
            authorization: `Bearer ${ACCESS_TOKEN}`,
            "content-type": "application/json"
          },
          body: JSON.stringify(todo)
        });
        if (response.ok) {
          this.setState({ newTodoText: "" });
        }
      }
    }
  }

  render() {
    const signedIn = this.props.auth.isAuthenticated();
    const { auth } = this.props;
    const { newTodoText, priv } = this.state;
    return (
      <div className="container mx-auto border-b-2 border-solid border-orange-darker">
        <div className="flex items-center justify-between my-5">
          <h1 className="flex-grow">ToDos</h1>
          <Link
            className="mx-5 text-xl text-header-color font-bold font-header no-underline"
            to="/"
          >
            Public
          </Link>
          {signedIn && (
            <Link
              className="mx-5 text-xl text-header-color font-bold font-header no-underline"
              to="/private"
            >
              Private
            </Link>
          )}
          {signedIn ? (
            <button
              className="mx-5 px-4 py-3 bg-orange-light rounded-lg shadow hover:shadow-md"
              onClick={auth.logout}
            >
              Log Out
            </button>
          ) : (
            <button
              className="mx-5 px-4 py-3 bg-orange-light rounded-lg shadow hover:shadow-md"
              onClick={auth.login}
            >
              Log In
            </button>
          )}
        </div>
        {signedIn && (
          <div className="flex items-center justify-center px-5 py-3 bg-orange-lightest">
            <div className="">
              <label
                className="text-grey-darkest px-2"
                htmlFor="add-todo-textbox"
              >
                New todo
              </label>
              <input
                id="add-todo-textbox"
                className="text-grey-darkest px-2 py-1"
                name="add-todo-textbox"
                type="text"
                value={newTodoText}
                onChange={e => this.setState({ newTodoText: e.target.value })}
              />
            </div>
            <div>
              <label
                className="text-grey-darkest px-2"
                htmlFor="add-todo-private-checkbox"
              >
                Private
              </label>
              <input
                id="add-todo-private-checkbox"
                type="checkbox"
                checked={priv}
                onChange={() => this.setState(state => ({ priv: !state.priv }))}
              />
            </div>
            <button
              className="mx-5 px-3 py-2 bg-orange rounded-lg shadow hover:shadow-md"
              onClick={() => this.addTodo()}
            >
              Add Todo
            </button>
          </div>
        )}
      </div>
    );
  }
}

export default TopBar;
