import React from "react";
import TodoList from "./TodoList";

class PublicTodos extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      todos: []
    };
    this.toggleDone = this.toggleDone.bind(this);
    this.deleteTodo = this.deleteTodo.bind(this);
  }

  componentDidMount() {
    this.getTodos();
  }

  async getTodos() {
    try {
      const response = await fetch("http://localhost:8080");
      const todos = response.ok && (await response.json());
      todos && this.setState({ todos: todos.result });
    } catch (err) {
      console.log(err);
    }
  }

  async toggleDone(todo) {
    try {
      await fetch("http://localhost:8080/", {
        mode: "cors",
        method: "PUT",
        headers: {
          "content-type": "application/json"
        },
        body: JSON.stringify({ ...todo, done: !todo.done })
      });
      this.getTodos();
    } catch (err) {
      console.log(err);
    }
  }

  async deleteTodo(todo) {
    try {
      await fetch("http://localhost:8080/", {
        mode: "cors",
        method: "DELETE",
        headers: {
          "content-type": "application/json"
        },
        body: JSON.stringify(todo)
      });
      this.getTodos();
    } catch (err) {
      console.log(err);
    }
  }

  render() {
    return (
      <div className="container mx-auto py-5">
        <div className="flex items-baseline">
          <h2 className="flex-grow">Public List</h2>
          <button
            className="mx-5 px-3 py-2 bg-orange rounded-lg shadow hover:shadow-md"
            onClick={() => this.getTodos()}
          >
            Refresh Public todos
          </button>
        </div>
        <TodoList
          todos={this.state.todos}
          toggleDone={this.toggleDone}
          deleteTodo={this.deleteTodo}
        />
      </div>
    );
  }
}
export default PublicTodos;
